class MyAssetIcons {
  static const String logo = 'assets/icons/logo.svg';
  static const String home = 'assets/icons/home.svg';
  static const String search = 'assets/icons/search.svg';
  static const String settings = 'assets/icons/settings.svg';
  static const String calendar = 'assets/icons/calendar.svg';
  static const String checked = 'assets/icons/checked.svg';
  static const String minus = 'assets/icons/minus.svg';
  static const String plus = 'assets/icons/plus.svg';
  static const String medicationBox = 'assets/icons/MedicationBox.svg';
  static const String corn = 'assets/icons/corn.svg';

}

class MyAnimalsImage {
  static const String cow = 'assets/animals_image/cow.png';
  static const String chicken = 'assets/animals_image/chicken.png';  
  static const String sheep = 'assets/animals_image/sheep.png';
  static const String hourse = 'assets/animals_image/hourse.png'; 
  static const String no_connection = 'assets/animals_image/no_connection_r.png'; 

}
